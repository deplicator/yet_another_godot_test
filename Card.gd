extends Area2D

var data

var can_grab = false
var moving = false
var moveable = false
var grabbed_offset = Vector2()

func _ready():
    $Face.frame = data.uid % 52
    set_process_input(true)
    

func _process(delta):
    # allow to move while holding mouse button
    if moving:
        position = get_global_mouse_position() + grabbed_offset
    


func _on_Card_mouse_entered():
    # number of cards on this tableu
    var top = 0
    if data.location != "stock":
        top = get_node("/root/Table").cards_on_tableau[data.tableau_index]
    
    # This is the top card
    if top == data.tableau_number:
        moveable = true
        print(data)
    else:
        moveable = false

func _on_Card_mouse_exited():
    can_grab = false
    moveable = false
    moving = false
        

func flip_card():
    $Face.visible = not $Face.visible
    $Back.visible = not $Back.visible


func _input(event):
    if event is InputEventMouseButton:
        if event.doubleclick:
            flip_card()
            get_tree().set_input_as_handled()

    if Input.is_action_just_released("mouse_left"):
        # snap to position
        position = get_global_mouse_position() + grabbed_offset + Vector2(0, 20)
        moving = false
        moveable = false
        get_tree().set_input_as_handled()


func _unhandled_input(event):
    if moveable and (event is InputEventMouseButton) and (event.button_index == BUTTON_LEFT):

        if event.pressed and not event.is_echo():

            if $Face.get_rect().has_point(get_local_mouse_position()):

                moving = true
                grabbed_offset = position - get_global_mouse_position()

                # z index to make surec card graphic is on top
                get_node("/root/Table").colliding_highest_z += 1
                set_z_index(get_node("/root/Table").colliding_highest_z)
                $Face.set_z_index(get_node("/root/Table").colliding_highest_z)

                # reareange the children index so when click happens it happens on the top card.
                get_parent().move_child(self, get_parent().get_child_count())

                # don't want subsequent input callbacks to respsond to this input
                get_tree().set_input_as_handled() 








